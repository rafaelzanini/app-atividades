$(document).ready(function() {
    $.get("view.php", function(data) {
        $("#table_content").html(data);
    });

    $('#link-add').hide();
	$("#link-edit").hide();
	
    $('#show-add').click(function() {
        $('#link-add').slideDown(500);
        $('#show-add').hide();
    });

    $('#add').click(function() {        
		var nome = $('#nome').val();
        var descricao = $('#descricao').val();
        var data_inicio = $('#data_inicio').val();		
		var data_fim = $('#data_fim').val();
		var fk_status = $('#fk_status').val();
		var situacao = $('#situacao').val();

		if (nome == ''|| descricao == '' || data_inicio == '' || (!data_fim && fk_status =='4') || fk_status =='') {
			var msg_nome = ((!nome)?"Nome\n":"");
			var msg_descricao = ((!descricao)?'Descrição\n':'');
			var msg_data_inicio = ((!data_inicio)?'Data de inicio\n':'');
			var msg_data_fim = ((!data_fim && fk_status =='4')?'Data fim, pois o status da atividade está como finalizado\n':'');
			var msg_status = ((!fk_status)?'Status\n':'');
			alert('Os campos informados abaixo são obrigatórios\n'+msg_nome+msg_descricao+msg_data_inicio+msg_data_fim+msg_status);
			return false;
		}

        $.ajax({
            url: "add.php",
            type: "POST",
            data: { nome: nome, descricao: descricao, data_inicio: data_inicio, data_fim: data_fim, fk_status: fk_status, situacao: situacao },
            success: function(data, status, xhr) {
                $('#nome').val('');
                $('#descricao').val('');
                $('#data_inicio').val('');
				$('#data_fim').val('');
				$('#fk_status').val('');
				$('#situacao').val('');
				
                $.get("view.php", function(html) {
                    $("#table_content").html(html);
                });
                $('#records_content').fadeOut(1100).html(data);
            },
            error: function() {
                $('#records_content').fadeIn(3000).html('<div class="text-center">error here</div>');
            },
            beforeSend: function() {
                $('#records_content').fadeOut(700).html('<div class="text-center">Loading...</div>');
            },
            complete: function() {
                $('#link-add').hide();
                $('#show-add').show(700);
            }
        });
    });
	
	$('#search').click(function() {
		var search_status = $('#search_status').val();
		var search_situacao = $('#search_situacao').val();

		$.get("view.php?status="+search_status+"&situacao="+search_situacao, function(data) {
			$("#table_content").html(data);
		});        
    });

	$('#search_clear').click(function() {
			$.get("view.php", function(data) {
				$("#table_content").html(data);
			});
			$('#search_status').val("");
			$('#search_situacao').val("");        
    });

});
