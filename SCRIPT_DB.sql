CREATE TABLE `status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1

CREATE TABLE `atividades` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` text,
  `data_inicio` date NOT NULL,
  `data_fim` date NULL DEFAULT NULL,
  `fk_status` int NOT NULL,
  `situacao` tinyint(1) NOT NULL,  
  PRIMARY KEY (`id`),
  FOREIGN KEY (`fk_status`) REFERENCES status (id)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1

INSERT INTO `status` (`id`, `nome`) VALUES (1, 'Pendente'), (2, 'Em Desenvolvimento'), (3, 'Em Teste'), (4, 'Concluido');