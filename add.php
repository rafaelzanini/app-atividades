<?php

	require_once('dbconfig.php');
	global $con;

	$nome = $_POST['nome'];
	$descricao = $_POST['descricao'];
	$data_inicio = $_POST['data_inicio'];
	$data_fim = (empty($_POST['data_fim']))?null:$_POST['data_fim'];
	$fk_status = $_POST['fk_status'];
	$situacao = $_POST['situacao'];

	if (empty($nome) || empty($descricao) || empty($data_inicio) || empty($fk_status) || ($fk_status==4 && empty($data_fim))) {
		 echo '<div class="col-md-offset-4 col-md-5 text-center alert alert-danger">Erro ao realizar a inserção!</div>';
		 exit;
	} else {
		$sql = "INSERT into atividades (nome, descricao, data_inicio, data_fim, fk_status, situacao) ";
		if ($data_fim) {
			$sql .= " VALUES ('$nome', '$descricao', '$data_inicio', '$data_fim', $fk_status, $situacao)";
		} else {
			$sql .=" VALUES ('$nome', '$descricao', '$data_inicio', null, $fk_status, $situacao)";
		}
		$query = $con->prepare($sql);		
		$result = $query->execute();
		if($result) {
	        echo '<div class="col-md-offset-4 col-md-5 text-center alert alert-success">Atividade Inserida!</div>';
	    } else {
	    	exit(mysqli_error($con));
		}
	}