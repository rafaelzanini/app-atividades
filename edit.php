<?php

	require_once('dbconfig.php');
	global $con;
	$id = $_POST['id'];

	if(empty($id))
	{?>
		<div class="text-center">Erro ao buscar atividade no sistema <a href="#" onclick="$('#link-add').hide();$('#show-add').show(700);">Fechar</a></div>
	<?php
	}

	$query = "SELECT * FROM status order by id";
	if (!$resultStatus = mysqli_query($con, $query)) {
		exit(mysqli_error($con));
	}	
	
	$query = "SELECT * FROM atividades where id='$id'";
	if (!$result = mysqli_query($con, $query)) {
	        exit(mysqli_error($con));
	    }
	while($row = mysqli_fetch_assoc($result))
	{
		$readonly = ($row['fk_status'] == 4)?'disabled':'';
		?>
		<div id="edit-data" class="form">
			<div class="form-group row">
				<label for="update_nome"  class="col-sm-2 col-form-label">Nome</label>
				<div class="col-sm-10">
					<input type="text" name="nome" id="update_nome" value="<?php echo $row['nome']; ?>" class="form-control" required <?php echo $readonly;?>/>
				</div>
			</div>
			<div class="form-group row">
				<label for="update_descricao"  class="col-sm-2 col-form-label">Descri&ccedil;&atilde;o</label>
				<div class="col-sm-10">
					<textarea name="descricao"  id="update_descricao"	 rows="3" class="form-control"  required <?php echo $readonly;?>><?php echo $row['descricao']; ?></textarea>
				</div>
			</div>
			<div class="input-group row">
				<label for="update_nome"  class="col-sm-4 col-form-label">Data Inicio</label>
				<div class="col-sm-8">
					<input type="date" name="data_inicio" id="update_data_inicio" value="<?php echo $row['data_inicio']; ?>" class="form-control" required <?php echo $readonly;?>/>
				</div>
			</div><br>								
			<div class="input-group row">
				<label for="update_nome" class="col-sm-4 col-form-label">Data Fim</label>
				<div class="col-sm-8">
					<input type="date" name="data_fim" id="update_data_fim" value="<?php echo $row['data_fim']; ?>" class="form-control" <?php echo $readonly;?> />
				</div>
			</div><br>
			<div class="form-group row">
				<label for="update_fk_status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-8">
					<select name="fk_status" id="update_fk_status" class="form-control selectpicker" required <?php echo $readonly;?>>
						<option>--Selecione--</option>
						<?php while($status = mysqli_fetch_object($resultStatus))
						   {
							  $selected =($status->id == $row['fk_status'])?'selected':'';
							  echo "<option value='{$status->id}' $selected >{$status->nome}</option>";
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="update_fk_status" class="col-sm-2 col-form-label">Situa&ccedil;&atilde;o</label>
				<div class="col-sm-8">
					<select name="situacao" id="update_situacao" class="form-control selectpicker" required <?php echo $readonly;?>>
						
						<option value='1' <?php print ($row['situacao'] == 1)?'selected':''; ?>>Ativo</option>
						<option value='2' <?php print ($row['situacao'] == 0)?'selected':''; ?>>Inativo</option>
					</select>
				</div>
			</div>
		</div>
		<?php if ($row['fk_status'] == 4) { ?>
			<button type="button" href="javascript:void(0);" class="btn btn-default" id="cancel"  data-dismiss="modal" name="add" onclick="$('#link-add').slideUp(400);$('#show-add').show(700);$('link-add').html();">Fechar</button>
		<?php } else { ?>			
			<button type="button" class="btn btn-primary update" id="<?php echo $row['id']; ?>"  data-dismiss="modal" name="update" >Atualizar</button>
			<button type="button" href="javascript:void(0);" class="btn btn-default" id="cancel"  data-dismiss="modal" name="add" onclick="$('#link-add').slideUp(400);$('#show-add').show(700);$('link-add').html();">Cancelar</button>
		<?php } ?>
	</div>
	<?php
	}
	?>

<script type="text/javascript">
	$('.update').click(function() {		
		var id = $(this).attr('id');
        var nome = $('#update_nome').val();
        var descricao = $('#update_descricao').val();
        var data_inicio = $('#update_data_inicio').val();		
		var data_fim = $('#update_data_fim').val();
		var fk_status = $('#update_fk_status').val();
		var situacao = $('#update_situacao').val();
		
		if (nome == ''|| descricao == '' || data_inicio == '' || (!data_fim && fk_status =='4')) {
			var msg_nome = ((!nome)?"Nome\n":"");
			var msg_descricao = ((!descricao)?'Descrição\n':'');
			var msg_data_inicio = ((!data_inicio)?'Data de inicio\n':'');
			var msg_data_fim = ((!data_fim && fk_status =='4')?'Data fim, pois o status da atividade está como finalizado':'');
			var msg_status = ((!fk_status)?'Status\n':'');
			alert('Os campos informados abaixo são obrigatórios\n'+msg_nome+msg_descricao+msg_data_inicio+msg_data_fim+msg_status);
			return false;
		}
		
        $.ajax({
            url: "update.php",
            type: "POST",
			data: { id: id, nome: nome, descricao: descricao, data_inicio: data_inicio, data_fim: data_fim, fk_status: fk_status, situacao: situacao },
            success: function(data, status, xhr) {
				$('#nome').val('');
                $('#descricao').val('');
                $('#data_inicio').val('');
				$('#data_fim').val('');
				$('#fk_status').val('');
				$('#situacao').val('');			
                $('#records_content').fadeOut(1100).html(data);
                $.get("view.php", function(html) {
                    $("#table_content").html(html);
                });
                $('#records_content').fadeOut(1100).html(data);
            },
            complete: function() {
                $('#link-edit').hide();
                $('#show-add').show(700);
				
            }
        });
    });
</script>