<?php
require_once('dbconfig.php');
global $con;
$query = "SELECT * FROM status order by id";
if (!$result = mysqli_query($con, $query)) {
		exit(mysqli_error($con));
	}	
	
	if (!$result2 = mysqli_query($con, $query)) {
		exit(mysqli_error($con));
	}	
?>
<html>
<head>
    <title>Gestão de Atividades</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
</head>
<body>
    <div class="container-fluid" style="padding: 0px; margin: 0px;">
        <div class="jumbotron">
            <h1 class="text-center">Gestão de Atividades</h1>
        </div>
    </div>
    <div class="container" style="padding-top: 25px; width: auto">
        <div class="row" style=" width: 82%; margin-left: 9%;">
            <div class="">
				<div class="form-inline pull-left">
						<select name="search_situacao" id="search_situacao" class="form-control selectpicker">
							<option value=''></option>
							<option value='1'>Ativo</option>
							<option value='2'>Inativo</option>
						</select>
						<select name="search_status" id="search_status" class="form-control selectpicker">
							<option value=' '></option>
								<?php while($status = mysqli_fetch_object($result))
								   {
									echo "<option value='{$status->id}'>{$status->nome}</option>";
									}
								?>
						</select>
                    <button class="btn btn-warning" id="search" name="search">Pesquisar</button>
					<button class="btn btn-primary" id="search_clear" name="search_clear">Limpar</button>
                </div>
                <div class="pull-right">
                    <button class="btn btn-success add" id="show-add" data-toggle="modal" data-target="#myModal" name="add">Adicionar nova Atividade</button>
                </div>				
				  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
					<div class="modal-dialog">
					  <div class="modal-content">
						<div class="modal-header">
						  <h4 class="modal-title">Gestão de Atividade</h4>
						</div>
						<div class="modal-body">						  
							<div id="link-add" class="form">
								<div class="form-group row">
									<label for="nome"  class="col-sm-2 col-form-label">Nome</label>
									<div class="col-sm-10">
										<input type="text" name="nome" id="nome" placeholder="Nome" class="form-control" maxlength="255" />
									</div>
								</div>
								<div class="form-group row">
									<label for="nome"  class="col-sm-2 col-form-label">Descri&ccedil;&atilde;o</label>
									<div class="col-sm-10">
										<textarea name="descricao"  id="descricao" placeholder="Descri&ccedil;&atilde;o" rows="3" class="form-control" maxlength="600"></textarea>
									</div>
								</div>
								<div class="input-group row">
									<label for="nome"  class="col-sm-4 col-form-label">Data Inicio</label>
									<div class="col-sm-8">
										<input type="date" name="data_inicio" id="data_inicio" placeholder="Data Inicio" class="form-control" />
									</div>
								</div><br>								
								<div class="input-group row">
									<label for="nome" class="col-sm-4 col-form-label">Data Fim</label>
									<div class="col-sm-8">
										<input type="date" name="data_fim" id="data_fim" placeholder="Data Fim" class="form-control" />
									</div>
								</div><br>
								<div class="form-group row">
									<label for="fk_status" class="col-sm-2 col-form-label">Status</label>
									<div class="col-sm-8">
										<select name="fk_status" id="fk_status" class="form-control selectpicker" required>
											<option></option>
											<?php while($status = mysqli_fetch_object($result2))
											   {
												echo "<option value='{$status->id}'>{$status->nome}</option>";
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="fk_status" class="col-sm-2 col-form-label">Situa&ccedil;&atilde;o</label>
									<div class="col-sm-8">
										<select name="situacao" id="situacao" class="form-control selectpicker" required>
											<option value='1'>Ativo</option>
											<option value='0'>Inativo</option>
										</select>
									</div>
								</div>
									<button type="button" class="btn btn-primary" id="add" name="add" data-dismiss="modal">Adicionar</button>
									<button type="button" href="javascript:void(0);" class="btn btn-default" id="cancel" name="add" data-dismiss="modal" onclick="$('#link-add').slideUp(400);$('#show-add').show();">Cancelar</button>						  
							</div>
							<div id="link-edit" class="form">
							</div>
						</div>
					  </div>					  
					</div>
				</div>				
            </div>
        </div>	
        <div class="row">
            <div class="col-md-12">
                <div id="records_content"></div>
                <br>
                <div class="col-md-offset-1 col-md-10" id="table_content">
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min"></script>
    <script type="text/javascript" src="app.js"></script>
</body>
</html>