<?php
	require_once('dbconfig.php');
	global $con;
	
	$where = '';
	
	if (isset($_GET['status']) AND !empty($_GET['status']) OR (isset($_GET['situacao']) AND !empty($_GET['situacao']))) {
		$where = " WHERE";
		if ($_GET['status']) {
			$where .= " fk_status = ".$_GET['status'];
		}
		
		if (!empty($_GET['status'] && !empty($_GET['situacao']))) {
			$where .= " AND";
		}
		
		if ($_GET['situacao']) {
			$where .= " situacao = ".$_GET['situacao'];
		}
	}	
	
	$query = $con->prepare(
				"SELECT a.id, a.nome as nome, descricao, data_inicio, data_fim, situacao, fk_status, s.nome as status
				FROM atividades a
				JOIN status s ON (s.id = a.fk_status)
				$where 
				order by a.id ASC"
			);

	$query->execute();
	mysqli_stmt_bind_result($query, $id, $nome, $descricao, $data_inicio, $data_fim, $situacao,	$fk_status, $status);
	
	?>
	<table class="table table-bordered ">
		<tr class="info">
			<th>ID</th>
			<th>Nome</th>
			<th>Descri&ccedil;&atilde;o</th>
			<th>Data Inicio</th>
			<th>Data Fim</th>
			<th>Status</th>
			<th>Situa&ccedil;&atilde;o</th>
			<th>Ferramentas</th>
		</tr>
	<?php

	while(mysqli_stmt_fetch($query))
	{
		$class = ($fk_status == 4)?'danger':'';
		$situacao = ($situacao == 1)?'Ativo':'Inativo';
		$texto = ($fk_status == 4)?'Visualizar':'Editar';
		echo "
		<tr class='$class'>
			<td>$id</td>
			<td>$nome</td>
			<td>$descricao</td>
			<td>$data_inicio</td>
			<td>$data_fim</td>
			<td>$status</td>
			<td>$situacao</td>
			<td><button id='$id' class='edit btn btn-info' data-toggle='modal' data-target='#myModal'>$texto</button></td>
		</tr>";
	}
		echo '</table>';
	
?>
<script type="text/javascript">
	$('.edit').click(function() {
		var id = $(this).attr('id');
		$('#show-add').hide();
		$.ajax({
	    url : 'edit.php',
	    type: 'POST',
	    data : { id: id },
	    success: function(data)
	    {
    		$("#link-edit").html(data);
    		$('#link-edit').show();
	    }
	});
});

$('.add').click(function() {
	$("#link-edit").empty();
});

</script>