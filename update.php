<?php
	require_once('dbconfig.php');
	global $con;

	$id = $_POST['id'];
	$nome = $_POST['nome'];
	$descricao = $_POST['descricao'];
	$data_inicio = date("Y-m-d", strtotime($_POST['data_inicio']));
	$data_fim = (empty($_POST['data_fim']))?null:date("Y-m-d", strtotime($_POST['data_fim']));
	$fk_status = $_POST['fk_status'];
	$situacao = $_POST['situacao'];

	if (empty($nome) || empty($descricao) || empty($data_inicio) || empty($fk_status) || ($fk_status==4 && empty($data_fim))) {
		 echo '<div class="col-md-offset-4 col-md-5 text-center alert alert-danger">Erro ao realizar a atualização!</div>';
		 exit;
	} else {
		$sql = "UPDATE atividades  SET nome='$nome', descricao='$descricao', data_inicio='$data_inicio', ";			
		if ($data_fim) {
			$sql .=" data_fim='$data_fim', fk_status='$fk_status', 	situacao='$situacao' WHERE id=$id";
		} else {
			$sql .=" data_fim=null, fk_status='$fk_status', situacao='$situacao' WHERE id=$id";
		}

		$query = $con->prepare($sql);
		$result = $query->execute();		
		if($result) {
	        echo '<div class="col-md-offset-4 col-md-5 text-center alert alert-success">Atualizado com sucesso!</div>';
	    } else {
	    	exit(mysqli_error($con));
		}
	}